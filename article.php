<?php
require 'include/database.php';
require 'include/article-related.php';
//Get database connection

$conn = getDB();

if (isset($_GET['id'])) {

    $article = getArticle($conn, $_GET['id']);
} else {
    $article = null;
}

?>

<?php require 'include/header.php'; ?>
<body>

<h1>Chad Blog!</h1>
<form action="index.php">
    <button type="submit" >Go back to home</button>
</form>
<?php if ($article === false): ?>

    <p>No article found on this planet and probably universe.</p>
<?php else: ?>
    <a href="edit-article.php?id=<?= htmlspecialchars($article['id']); ?>"><button>Edit article</button></a>
    <a href="delete-article.php?id=<?= htmlspecialchars($article['id']); ?>"><button>Delete article</button></a>

    <article>
        <h2><?= htmlspecialchars($article['title']); ?></h2>
        <p><?= htmlspecialchars($article['content']); ?></p>
    </article>


<?php endif; ?>

</body>
<?php require 'include/footer.php'; ?>
