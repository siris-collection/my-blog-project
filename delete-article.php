<?php
require 'include/database.php';
require 'include/article-related.php';

$conn = getDB();

if (isset($_GET['id'])) {

    $article = getArticle($conn, $_GET['id'], 'id');
    $dateTime = getDateTime($conn, $_GET['id']);

    $id = $article['id'];

} else {
    die("id not supplied, article not found");
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $sql = "DELETE FROM sirivat 
        WHERE id = $1";

    $stmt = pg_prepare($conn, "deleteQuery", $sql);

    if ($stmt === false) {
        pg_last_error($conn);
    } else {
        pg_execute($conn, "deleteQuery", array($id));
        header("Location: /index.php");
    }
}

?>

<?php require 'include/header.php'; ?>

    <h2>Delete article</h2>

<p>Are you sure you want to delete this article?</p>

<form method="POST">
    <button>Delete</button>
    <a href="article.php?id= <?= $article['id']; ?> ">Cancel</a>
</form>

<?php require 'include/footer.php'; ?>