<?php
require 'include/database.php';
require 'include/article-related.php';

$conn = getDB();

if (isset($_GET['id'])) {

    $article = getArticle($conn, $_GET['id']);
    $dateTime = getDateTime($conn, $_GET['id']);

    $id = $article['id'];
    $title = $article['title'];
    $content = $article['content'];
    $published_at = $dateTime['to_char'];

} else {
    die("id not supplied, article not found");
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST['title'];
    $content = $_POST['content'];

    $errors = validateForm($title, $content);

    if(empty($errors)) {
        $sql = "UPDATE sirivat 
                SET title = $1,
                    content = $2,
                    published_at = current_timestamp
                WHERE id = $3";

        $stmt = pg_prepare($conn, "updateQuery" , $sql);

        if ($stmt === false) {
            echo pg_last_error($conn);
        } else {
            pg_execute($conn, "updateQuery", array($title,$content, $id));
        }
    }
}

?>

<?php require 'include/header.php' ?>
    <h2>Edit Article</h2>
    <form action="index.php">
        <button type="submit">Go back to home</button>
    </form>
    <a href="article.php?id=<?= htmlspecialchars($article['id']); ?>"><button>Go back to article</button></a>

    <p>Last updated on: <?php echo $published_at ?> </p>
<?php require 'include/article-form.php' ?>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
            crossorigin="anonymous"></script>

<?php require 'include/footer.php' ?>