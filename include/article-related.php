<?php

/*get article record based on id*/

function getArticle ($conn, $id, $column = '*') {
    $sqlGet = "SELECT $column FROM sirivat WHERE id = $1";

    $stmt = pg_prepare($conn, "getQuery", $sqlGet);

    if ($stmt === false){
        echo pg_last_error($conn);
    } else {
        $results = pg_execute($conn, "getQuery", array($id));
        return pg_fetch_assoc($results);
    }
}

function getDateTime ($conn, $id) {
    $sqlDate = "SELECT TO_CHAR(\"published_at\", 'DD-MM-YYYY HH24:MI:SS') FROM sirivat WHERE id = $1";
    $datePrepare = pg_prepare($conn, "dateQuery", $sqlDate);

    if ($datePrepare === false){
        echo pg_last_error($conn);
    } else {
        $dateResult = pg_execute($conn,"dateQuery", array($id));
        return pg_fetch_assoc($dateResult);
    }
}

/*validate article properties
@param string title, required
@param string content, required */

function validateForm ($title, $content) {
    $errors = [];

        if ($_POST['title'] == '') {
            $errors[] = "Title is required";
        }
        if ($_POST ['content'] == '') {
            $errors[] = "Content is required";
        }
        return $errors;

}