<?php
require 'include/database.php';
require 'include/auth.php';

session_start();

$conn = getDB();

$results = pg_query($conn, "SELECT * FROM sirivat ORDER BY id ");

if ($results === false) {
    echo "Some error in your query idiot";
} else {
    $articles = pg_fetch_all($results);
}

?>
<?php require 'include/header.php'; ?>
    <body>

    <h1>Blog for the universe</h1>

    <?php if (isLoggedIn()) : ?>

        <p>You are logged in. <a href="logout.php">Log out</a></p>
        <form action="new-article.php">
            <button type="submit">Create New Article</button>
        </form>

    <?php else: ?>

        <p>You are not logged in. <a href="login.php">Log in</a></p>

    <?php endif; ?>

    <ul>
        <?php foreach ($articles as $article): ?>
            <li>
                <article>
                    <h2><a href="article.php?id=<?= htmlspecialchars($article['id']); ?>"><?= $article['title']; ?></a>
                    </h2>
                    <p><?= htmlspecialchars($article['content']); ?></p>
                </article>
            </li>
        <?php endforeach; ?>
    </ul>


    </body>
<?php require 'include/footer.php'; ?>