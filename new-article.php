<?php

require 'include/database.php';
require 'include/article-related.php';
require 'include/auth.php';

session_start();

if (! isLoggedIn()){
    die("Unauthorized");
}

$title = '';
$content = '';
$published_at = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST['title'];
    $content = $_POST['content'];

    $errors = validateForm($title, $content);

    if (empty($errors)) {
        $conn = getDB();

        $sql = "INSERT INTO sirivat (title, content, published_at)
            VALUES ($1, $2, CURRENT_TIMESTAMP)
            RETURNING id
            ";

        // "$_POST['title']", "$_POST['content']", $_POST['published_at']

        $stmt = pg_prepare($conn, "prep", $sql);

        if ($stmt === false) {
            echo "Some error in your query idiot";
        } else {

            $results = pg_execute($conn, "prep", array($title, $content));

            $resultArray = pg_fetch_assoc($results);
            $id = $resultArray["id"];
            echo "Inserted record with ID: $id";
            header("Location: index.php");
        }
    }
}


?>

<?php require 'include/header.php' ?>

<h1>My Blog</h1>

<h2>New Article</h2>
<form action="index.php">
    <button type="submit">Go back to home</button>
</form>

<?php require 'include/article-form.php' ?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script>
<?php require 'include/footer.php' ?>

